<p align="center">
    <img src="logo.png" alt="Airway Logo" width="256" height="256" />
</p>

# Airway

Airway is a library intended to assist Java applications that use or want to use CQRS as part of their architectural
definition and are aiming to decouple commands and queries from their executors.

It provides routing between commands and queries and their respective handlers (in the same process) allowing you to
effectively decouple
the caller from the executor.

Airway also comes with event emitting and listening, supporting multiple event receivers (in the same process).

It is heavly inspired by [MediatR](https://github.com/jbogard/MediatR) in that it aims to be simple and unobtrusive.

Features:

- Clean and concise API
- Fully unit-tested
- Fully documented
- No external dependencies

## Examples:

**Commands**

```java
class CreateUserCommand implements Command<Boolean> {
    public final String name;
    public final String email;
    public final String password;
    /* constructor */
}

class CreateUserCommandHandler implements CommandHandler<CreateUserCommand, Boolean> {
    @Override
    Boolean handle(CreateUserCommand cmd) {
        /* create user */
        return true;
    }
}

public class Application {
    public static void main() {
        Airway airway = new InProcessAirway();
        airway.register(CreateUserCommand.class, new CreateUserCommandHandler());
        airway.dispatch(new CreateUserCommand("Test subject", "test@sub.ject", "12345"));
    }
}

```

**Events**

```java
class UserCreatedEvent implements Event {
    public final String name;
    public final String email;
    /* constructor */
}

public class Application {
    public static void main() {
        Airway airway = new InProcessAirway();
        airway.listen(UserCreatedEvent, event -> System.out.printl("User " + event.name + " created"));
        airway.emit(new UserCreatedEvent("Test subject", "test@sub.ject"));
    }
}
```

**Sprint boot integration:**

- Create an Airway bean:

```java
import tech.rocketwave.airway.InProcessAirway;

@Configuration
class AirwayConfiguration {
    @Bean
    Airway airway() {
        return new InProcessAirway();
    }
}
```

- Register domain specific commands and queries in `@Configuration` classes

```java
@Configuration
class UserAirwayConfiguration {
    public UserAirwayConfiguration(Airway airway) {
        airway.register(RegisterUserCommand.class, new RegisterUserCommaandHandler());
        airway.register(ChangeUserAddressCommand.class, new ChangeUserAddressCommandHandler());
        airway.register(GetUserByEmailQuery.class, new ChangeUserAddressQueryHandler());
    }
}
```
