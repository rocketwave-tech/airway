package tech.rocketwave.airway;


import tech.rocketwave.airway.command.Command;
import tech.rocketwave.airway.command.CommandDispatcher;
import tech.rocketwave.airway.command.CommandHandler;
import tech.rocketwave.airway.event.Event;
import tech.rocketwave.airway.event.EventBus;
import tech.rocketwave.airway.event.EventListener;
import tech.rocketwave.airway.query.Query;
import tech.rocketwave.airway.query.QueryDispatcher;
import tech.rocketwave.airway.query.QueryHandler;

import java.util.Objects;

public class BareAirway implements Airway {
    private final CommandDispatcher commandDispatcher;
    private final QueryDispatcher queryDispatcher;
    private final EventBus eventBus;

    public BareAirway(
            CommandDispatcher commandDispatcher,
            QueryDispatcher queryDispatcher,
            EventBus eventBus
    ) {
        this.commandDispatcher = Objects.requireNonNull(commandDispatcher);
        this.queryDispatcher = Objects.requireNonNull(queryDispatcher);
        this.eventBus = Objects.requireNonNull(eventBus);
    }

    @Override
    public <CommandT extends Command<ReturnT>, ReturnT> void register(Class<CommandT> commandClass, CommandHandler<CommandT, ReturnT> handler) {
        this.commandDispatcher.register(commandClass, handler);
    }

    @Override
    public <QueryT extends Query<ReturnT>, ReturnT> void register(Class<QueryT> queryClass, QueryHandler<QueryT, ReturnT> handler) {
        this.queryDispatcher.register(queryClass, handler);
    }

    @Override
    public <EventT extends Event> void listen(Class<EventT> eventClass, EventListener<EventT> listener) {
        this.eventBus.listen(eventClass, listener);
    }

    @Override
    public <ReturnT> ReturnT dispatch(Query<ReturnT> query) {
        return this.queryDispatcher.dispatch(query);
    }

    @Override
    public <ReturnT> ReturnT dispatch(Command<ReturnT> command) {
        return this.commandDispatcher.dispatch(command);
    }

    @Override
    public <EventT extends Event> void emit(EventT event) {
        this.eventBus.emit(event);
    }
}
