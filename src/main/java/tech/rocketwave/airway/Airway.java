package tech.rocketwave.airway;


import tech.rocketwave.airway.command.Command;
import tech.rocketwave.airway.command.CommandHandler;
import tech.rocketwave.airway.event.Event;
import tech.rocketwave.airway.event.EventListener;
import tech.rocketwave.airway.query.Query;
import tech.rocketwave.airway.query.QueryHandler;


/**
 * The Airway interface is the focal point of the Airway library. It specifies
 * the requirements for its implementations and the contract to users, providing
 * specifications for a. registering commands and queries' handlers, b. dispatching
 * those commands to their handlers, c. emitting events and d. listening to emitted events.
 */
public interface Airway {
    /**
     * Register a new handler for a command. If there is an existing handler it will be overridden.
     * The {@link CommandHandler#handle handle} method of the handler will be called when the command gets dispatched
     * via {@link #dispatch(Command) dispatch} method.
     *
     * @param commandClass The class of the command, must implement the {@link Command} interface
     * @param handler      A handler for the command, must implement the {@link CommandHandler} interface
     * @param <CommandT>   The command type
     * @param <ReturnT>    The return type of the command
     */
    <CommandT extends Command<ReturnT>, ReturnT> void register(Class<CommandT> commandClass, CommandHandler<CommandT, ReturnT> handler);

    /**
     * Register a new handler for a query. If there is an existing handler it will be overridden.
     * The {@link QueryHandler#handle handle} method of the handler will be called when the query gets dispatched
     * via {@link #dispatch(Query) dispatch} method.
     * @param queryClass The class of the query, must implement the {@link Query} interface
     * @param handler A handler for the query, must implement the {@link QueryHandler} interface
     * @param <QueryT> The query type
     * @param <ReturnT> The return type of the query
     */
    <QueryT extends Query<ReturnT>, ReturnT> void register(Class<QueryT> queryClass, QueryHandler<QueryT, ReturnT> handler);

    /**
     * Register a listener for an event. It is not necessary to previously register the event type.
     * @param eventClass the class of the event
     * @param listener the listener that will get called every time the event is emitted
     * @param <EventT> the type of the event
     */
    <EventT extends Event> void listen(Class<EventT> eventClass, EventListener<EventT> listener);

    /**
     * Dispatches a {@link Query query} to its handler. If no handler is found, an {@link IllegalStateException} is thrown.
     * @param query Query to dispatch, it must implement {@link Query} interface
     * @return matching handler's {@link QueryHandler#handle handle} return object
     * @param <ReturnT> return type of the query
     */
    <ReturnT> ReturnT dispatch(Query<ReturnT> query);

    /**
     * Dispatches a {@link Command command} to its handler. If no handler is found, an {@link IllegalStateException} is thrown.
     * @param command Command to dispatch, it must implement {@link Command} interface
     * @return matching handler's {@link CommandHandler#handle handle} return object
     * @param <ReturnT> return type of the command
     */
    <ReturnT> ReturnT dispatch(Command<ReturnT> command);

    /**
     * Emits an event to all its listeners. Listeners are registered with the {@link #listen} method.
     * @param event event to dispatch. It must implement the {@link Event} interface
     * @param <EventT> type of the event
     */
    <EventT extends Event> void emit(EventT event);
}
