package tech.rocketwave.airway.event;

/**
 * The Event interface works like a marker for events. Events emitted with Airway must implement this interface.
 */
public interface Event {
}
