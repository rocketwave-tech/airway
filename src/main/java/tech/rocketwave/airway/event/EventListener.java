package tech.rocketwave.airway.event;

/**
 * Specifies the requirements for an {@link Event event} listener.
 *
 * @param <EventT> the event type that the listener will listen to.
 */
public interface EventListener<EventT extends Event> {
    /**
     * Specifies the requirements for the method that will be invoked on the listener whenever
     * the event is emitted.
     *
     * @param event the event
     */
    void handle(EventT event);
}
