package tech.rocketwave.airway.event;



/**
 * Defines methods that an event bus should have.
 * <p>
 * The job of an event bus is to be able to call all listeners of an event.
 */
public interface EventBus {
    /**
     * Register a listener for an event. It is not necessary to previously register the event type. It should be possible to register the same listener multiple times.
     *
     * @param eventClass the class of the event
     * @param listener   the listener of the event
     * @param <EventT>   the type of the event
     */
    <EventT extends Event> void listen(Class<EventT> eventClass, EventListener<EventT> listener);

    /**
     * Emits an event to all its listeners. Listeners are registered with the {@link #listen} method.
     *
     * @param event    event to dispatch. It must implement the {@link Event} interface
     * @param <EventT> type of the event
     */
    <EventT extends Event> void emit(EventT event);

}
