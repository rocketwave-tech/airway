package tech.rocketwave.airway.command;

import tech.rocketwave.airway.Airway;

/**
 * The command interface works like a marker for commands, serving the purpose of
 * declaring the command return type for potential command handlers. It also allows
 * for the correct use of the {@link Airway} interface and its implementations.
 *
 * @param <ReturnT> return type for the handlers {@link CommandHandler#handle(Command)} )} method
 */
public interface Command<ReturnT> {
}
