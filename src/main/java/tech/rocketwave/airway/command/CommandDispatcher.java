package tech.rocketwave.airway.command;



/**
 * Defines methods that a command dispatcher should have.
 */
public interface CommandDispatcher {
    /**
     * Register a new handler for a command. If there is an existing handler it must be overridden so that only one handler is called.
     * The {@link CommandHandler#handle handle} method of the registered handler must be called when the command gets dispatched
     * via {@link #dispatch(Command) dispatch} method.
     *
     * @param commandClass The class of the command, must implement the {@link Command} interface
     * @param handler      A handler for the command, must implement the {@link CommandHandler} interface
     * @param <CommandT>   The command type
     * @param <ReturnT>    The return type of the command
     */
    <CommandT extends Command<ReturnT>, ReturnT> void register(Class<CommandT> commandClass, CommandHandler<CommandT, ReturnT> handler);

    /**
     * Implementations must dispatch a {@link Command command} to its handler. If no handler is found, an {@link IllegalStateException} must be thrown.
     *
     * @param command   Command to dispatch, it must implement {@link Command} interface
     * @param <ReturnT> return type of the command
     * @return matching handler's {@link CommandHandler#handle handle} return object
     */
    <ReturnT> ReturnT dispatch(Command<ReturnT> command);

}
