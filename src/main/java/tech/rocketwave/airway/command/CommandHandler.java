package tech.rocketwave.airway.command;


import tech.rocketwave.airway.Airway;

/**
 * Specifies the requirements that a command handler should adhere to be {@link Airway#register(Class, CommandHandler) registered}
 * in Airway.
 *
 * @param <CommandT> The command type that this handler is for
 * @param <ReturnT>  The return type of the {@link #handle} method. It must match the command return type.
 */
public interface CommandHandler<CommandT extends Command<ReturnT>, ReturnT> {
    /**
     * Specifies the handle signature, including the return type and the argument type
     *
     * @param cmd The command to handle
     * @return The result of handling the command
     */
    ReturnT handle(CommandT cmd);
}
