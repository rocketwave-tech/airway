package tech.rocketwave.airway.query;

import tech.rocketwave.airway.Airway;

/**
 * Specifies the requirements that a query handler should adhere to be {@link Airway#register(Class, QueryHandler) registered}
 * in Airway.
 *
 * @param <QueryT>  The query type that this handler is for
 * @param <ReturnT> The return type of the {@link #handle} method. It must match the query return type.
 */
public interface QueryHandler<QueryT extends Query<ReturnT>, ReturnT> {

    /**
     * Specifies the handle signature, including the return type and the argument type
     *
     * @param query The quuery to handle
     * @return The result of handling the query
     */
    ReturnT handle(QueryT query);
}
