package tech.rocketwave.airway.query;

import tech.rocketwave.airway.Airway;

/**
 * The query interface works like a marker for queryies, serving the purpose of
 * declaring the query return type for potential query handlers. It also allows
 * for the correct use of the {@link Airway} interface and its implementations.
 *
 * @param <ReturnT> return type for the handlers {@link QueryHandler#handle(Query)} )} method
 */
public interface Query<ReturnT> {
}
