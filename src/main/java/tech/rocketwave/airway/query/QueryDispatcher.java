package tech.rocketwave.airway.query;



/**
 * Defines methods that a query dispatcher should have.
 */
public interface QueryDispatcher {
    /**
     * Register a new handler for a query. If there is an existing handler it must be overridden so that only one handler is called.
     * The {@link QueryHandler#handle handle} method of the handler will be called when the query gets dispatched
     * via {@link #dispatch(Query) dispatch} method.
     *
     * @param queryClass The class of the query, must implement the {@link Query} interface
     * @param handler    A handler for the query, must implement the {@link QueryHandler} interface
     * @param <QueryT>   The query type
     * @param <ReturnT>  The return type of the query
     */
    <QueryT extends Query<ReturnT>, ReturnT> void register(Class<QueryT> queryClass, QueryHandler<QueryT, ReturnT> handler);

    /**
     * Must dispatch a {@link Query query} to its handler. If no handler is found, an {@link IllegalStateException} must be thrown.
     *
     * @param query     Query to dispatch, it must implement {@link Query} interface
     * @param <ReturnT> return type of the query
     * @return matching handler's {@link QueryHandler#handle handle} return object
     */
    <ReturnT> ReturnT dispatch(Query<ReturnT> query);

}
