package tech.rocketwave.airway;


import tech.rocketwave.airway.command.Command;
import tech.rocketwave.airway.command.CommandDispatcher;
import tech.rocketwave.airway.command.CommandHandler;
import tech.rocketwave.airway.event.Event;
import tech.rocketwave.airway.event.EventBus;
import tech.rocketwave.airway.event.EventListener;
import tech.rocketwave.airway.query.Query;
import tech.rocketwave.airway.query.QueryDispatcher;
import tech.rocketwave.airway.query.QueryHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * An in-process, synchronous implementation of the Airway interface.
 */
public class InProcessAirway extends BareAirway {
    /**
     * Instantiates a new InProcessAirway
     */
    public InProcessAirway() {
        super(new InProcessCommandDispatcher(), new InProcessQueryDispatcher(), new InClassEventBus());
    }
}

class InProcessCommandDispatcher implements CommandDispatcher {
    private final Map<Class<?>, CommandHandler<?, ?>> commandHandlers;

    public InProcessCommandDispatcher() {
        this.commandHandlers = new HashMap<>();
    }

    @Override
    public <CommandT extends Command<ReturnT>, ReturnT> void register(Class<CommandT> commandClass, CommandHandler<CommandT, ReturnT> handler) {
        this.commandHandlers.put(commandClass, handler);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <ReturnT> ReturnT dispatch(Command<ReturnT> command) {
        CommandHandler<Command<ReturnT>, ReturnT> handler = (CommandHandler<Command<ReturnT>, ReturnT>) this.commandHandlers.getOrDefault(command.getClass(), null);
        if (handler == null)
            throw new IllegalStateException("No command handler for " + command.getClass() + " found.");
        return handler.handle(command);
    }
}

class InProcessQueryDispatcher implements QueryDispatcher {
    private final Map<Class<?>, QueryHandler<?, ?>> queryHandlers;

    public InProcessQueryDispatcher() {
        this.queryHandlers = new HashMap<>();
    }

    @Override
    public <QueryT extends Query<ReturnT>, ReturnT> void register(Class<QueryT> queryClass, QueryHandler<QueryT, ReturnT> handler) {
        this.queryHandlers.put(queryClass, handler);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <ReturnT> ReturnT dispatch(Query<ReturnT> query) {
        QueryHandler<Query<ReturnT>, ReturnT> handler = (QueryHandler<Query<ReturnT>, ReturnT>) this.queryHandlers.getOrDefault(query.getClass(), null);
        if (handler == null)
            throw new IllegalStateException("No query handler for " + query.getClass() + " found.");
        return handler.handle(query);
    }
}

class InClassEventBus implements EventBus {
    private final Map<Class<?>, List<EventListener<?>>> eventListeners;

    public InClassEventBus() {
        this.eventListeners = new HashMap<>();
    }

    @Override
    public <EventT extends Event> void listen(Class<EventT> eventClass, EventListener<EventT> listener) {
        eventListeners
                .computeIfAbsent(eventClass, (k) -> new ArrayList<>())
                .add(listener);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <EventT extends Event> void emit(EventT event) {
        var eventClass = event.getClass();
        eventListeners
                .computeIfAbsent(eventClass, (k) -> new ArrayList<>())
                .forEach((rawListener) -> {
                    var listener = ((EventListener<EventT>) rawListener);
                    listener.handle(event);
                });
    }
}
