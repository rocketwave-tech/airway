package tech.rocketwave.airway;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.rocketwave.airway.command.Command;
import tech.rocketwave.airway.command.CommandHandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

enum TestCommandHandlerDiscriminator {
    FIRST,
    SECOND
}
class TestCommand implements Command<TestCommandHandlerDiscriminator> { }

class FirstTestCommandHandler implements CommandHandler<TestCommand, TestCommandHandlerDiscriminator> {
    @Override
    public TestCommandHandlerDiscriminator handle(TestCommand command) {
        return TestCommandHandlerDiscriminator.FIRST;
    }
}

class SecondTestCommandHandler implements CommandHandler<TestCommand, TestCommandHandlerDiscriminator> {
    @Override
    public TestCommandHandlerDiscriminator handle(TestCommand command) {
        return TestCommandHandlerDiscriminator.SECOND;
    }
}



class InProcessAirwayCommandTest {
    @Test
    @DisplayName("Dispacth command without handler must throw IllegalStateException")
    void dispatchCommandWithoutHandlerMustThrowIllegalStateException() {
        var airway = new InProcessAirway();
        assertThrows(IllegalStateException.class, () -> airway.dispatch(new TestCommand()));
    }

    @Test
    @DisplayName("Dispatch command expected to work after handler registered")
    void dispatchCommandExpectedToWorkAfterHandlerRegistered() {
        var airway = new InProcessAirway();
        airway.register(TestCommand.class, new FirstTestCommandHandler());
        assertEquals(airway.dispatch(new TestCommand()), TestCommandHandlerDiscriminator.FIRST);
    }

    @Test
    @DisplayName("Register command handler expected to accept and work with lambdas")
    void registerCommandHandlerExpectedToAcceptAndWorkWithLambdas() {
        var airway = new InProcessAirway();
        airway.register(TestCommand.class, command -> TestCommandHandlerDiscriminator.FIRST);
        assertEquals(airway.dispatch(new TestCommand()), TestCommandHandlerDiscriminator.FIRST);
    }

    @Test
    @DisplayName("Second register of command handler expected to overwrite previous handler")
    void secondRegisterOfCommandHandlerExpectedToOverwritePreviousHandler() {
        var airway = new InProcessAirway();
        airway.register(TestCommand.class, new FirstTestCommandHandler());
        airway.register(TestCommand.class, new SecondTestCommandHandler());
        assertEquals(airway.dispatch(new TestCommand()), TestCommandHandlerDiscriminator.SECOND);
    }
}
