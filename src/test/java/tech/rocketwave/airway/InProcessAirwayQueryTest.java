package tech.rocketwave.airway;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.rocketwave.airway.query.Query;
import tech.rocketwave.airway.query.QueryHandler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

enum TestQueryHandlerDiscriminator {
    FIRST,
    SECOND
}
class TestQuery implements Query<TestQueryHandlerDiscriminator> { }

class FirstTestQueryHandler implements QueryHandler<TestQuery, TestQueryHandlerDiscriminator> {
    @Override
    public TestQueryHandlerDiscriminator handle(TestQuery query) {
        return TestQueryHandlerDiscriminator.FIRST;
    }
}

class SecondTestQueryHandler implements QueryHandler<TestQuery, TestQueryHandlerDiscriminator> {
    @Override
    public TestQueryHandlerDiscriminator handle(TestQuery query) {
        return TestQueryHandlerDiscriminator.SECOND;
    }
}



class InProcessAirwayQueryTest {
    @Test
    @DisplayName("Dispacth query without handler must throw IllegalStateException")
    void dispatchQueryWithoutHandlerMustThrowIllegalStateException() {
        var airway = new InProcessAirway();
        assertThrows(IllegalStateException.class, () -> airway.dispatch(new TestQuery()));
    }

    @Test
    @DisplayName("Dispatch query expected to work after handler registered")
    void dispatchQueryExpectedToWorkAfterHandlerRegistered() {
        var airway = new InProcessAirway();
        airway.register(TestQuery.class, new FirstTestQueryHandler());
        assertEquals(airway.dispatch(new TestQuery()), TestQueryHandlerDiscriminator.FIRST);
    }

    @Test
    @DisplayName("Register query handler expected to accept and work with lambdas")
    void registerQueryHandlerExpectedToAcceptAndWorkWithLambdas() {
        var airway = new InProcessAirway();
        airway.register(TestQuery.class, query -> TestQueryHandlerDiscriminator.FIRST);
        assertEquals(airway.dispatch(new TestQuery()), TestQueryHandlerDiscriminator.FIRST);
    }

    @Test
    @DisplayName("Second register of query handler expected to overwrite previous handler")
    void secondRegisterOfQueryHandlerExpectedToOverwritePreviousHandler() {
        var airway = new InProcessAirway();
        airway.register(TestQuery.class, new FirstTestQueryHandler());
        airway.register(TestQuery.class, new SecondTestQueryHandler());
        assertEquals(airway.dispatch(new TestQuery()), TestQueryHandlerDiscriminator.SECOND);
    }
}
