package tech.rocketwave.airway;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.rocketwave.airway.event.Event;
import tech.rocketwave.airway.event.EventListener;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TestEvent implements Event {
    public final UUID id;
    public TestEvent(UUID id) {
        this.id = id;
    }

    public TestEvent() {
        this(UUID.randomUUID());
    }
}

class TestEventListener implements EventListener<TestEvent> {
    public Integer handledCount = 0;
    public UUID lastEventId = null;

    @Override
    public void handle(TestEvent event) {
        handledCount++;
        this.lastEventId = event.id;
    }
}

public class InProcessAirwayEventTest {
    @Test
    @DisplayName("Listen before emit expected to not throw")
    void listenBeforeEmitExpectedToWork() {
        var airway = new InProcessAirway();
        assertDoesNotThrow(() -> airway.listen(TestEvent.class, new TestEventListener()));
    }

    @Test
    @DisplayName("Emit before listen expected to not throw")
    void emitBeforeListenExpectedToNotThrow() {
        var airway = new InProcessAirway();
        assertDoesNotThrow(() -> airway.emit(new TestEvent()));
    }

    @Test
    @DisplayName("Emit before listen expected to not re-emit")
    void emitBeforeListenExpectedToNotReEmit() {
        var airway = new InProcessAirway();
        var listener = new TestEventListener();
        var id = UUID.randomUUID();
        airway.emit(new TestEvent(id));
        airway.listen(TestEvent.class, listener);
        assertEquals(listener.handledCount, 0);
        assertNotEquals(listener.lastEventId, id);
    }

    @Test
    @DisplayName("Emit after listen expected to work")
    void emitAfterListenExpectedToWork() {
        var airway = new InProcessAirway();
        var listener = new TestEventListener();
        var id = UUID.randomUUID();
        airway.listen(TestEvent.class, listener);
        airway.emit(new TestEvent(id));
        assertEquals(listener.lastEventId, id);
    }

    @Test
    @DisplayName("Listen expected to accept and work with lambdas")
    void listenExpectedToAcceptAndWorkWithLambdas() {
        var airway = new InProcessAirway();
        var id = UUID.randomUUID();
        var idHolder = new Object() {
            UUID emittedId = null;
        };
        airway.listen(TestEvent.class, event -> idHolder.emittedId = event.id);
        airway.emit(new TestEvent(id));
        assertEquals(idHolder.emittedId, id);
    }


    @Test
    @DisplayName("Emit expected to notify all listeners")
    void emitExpectedToNotifyAllListeners() {
        var airway = new InProcessAirway();
        var r = new Random();
        var listenerCount = r.nextInt(1024);
        var listeners = new ArrayList<TestEventListener>();
        var eventId = UUID.randomUUID();

        for (int i = 0; i < listenerCount; i++) {
            var listener = new TestEventListener();
            listeners.add(listener);
            airway.listen(TestEvent.class, listener);
        }
        airway.emit(new TestEvent(eventId));

        for (int i = 0; i < listenerCount; i++) {
            assertEquals(listeners.get(i).lastEventId, eventId);
        }
    }

    @Test
    @DisplayName("Listen expected to accept repeated listeners")
    void listenExpectedToAcceptRepeatedListeners() {
        var airway = new InProcessAirway();
        var r = new Random();
        var expectedCount = r.nextInt(1024);
        var listener = new TestEventListener();
        for (int i = 0; i < expectedCount; i++) {
            airway.listen(TestEvent.class, listener);
        }
        airway.emit(new TestEvent());
        assertEquals(listener.handledCount, expectedCount);
    }
}
